export default class Emitter {
    constructor() { 
        this.callbacks  = [];
        this.value;
        this.exist = false;
        this.concatOperations = [];
    }

    subscribe(eventName, fn) {
        this.exist = false;        
        this.exist = this.checkIfExistEvent(eventName, fn);
        if (!this.exist) {
            let dataCallbacks = {
                eventName,
                fn
            };
            this.callbacks.push(dataCallbacks);     
            return () => this.subscribe(eventName, fn);
        } else {
            this.callbacks.splice(0, this.callbacks.length - 1);
        }                        
    }

    checkIfExistEvent(eventName, fn) {        
        var exist = false;
        this.callbacks.forEach(element => {
            if (eventName === element.eventName && fn.toString() === element.fn.toString()) {
                exist = true;
            }
        });
        return exist;
    }

    emit (strName, value) {
        if (value) {
            if (this.callbacks.length > 0) {    
                this.callbacks.forEach(element => {
                    if (strName === element.eventName) {
                        this.value = value;
                        element.fn(this.value);
                    }
                });
            }
        }  else {
            this.callbacks.forEach(element => {
                if (strName === element.eventName) {
                    this.concatOperations.push(element.fn())
                }
            });
            this.value = value;
            return this.concatOperations;
        }  
    }
}
